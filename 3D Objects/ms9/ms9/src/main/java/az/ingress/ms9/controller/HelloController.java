package az.ingress.ms9.controller;

import az.ingress.ms9.dto.StudentDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(@RequestBody StudentDto dto, @RequestHeader("Accept-Language") String lang){
        switch (lang) {
            case "az":
            return dto.getName()+"-den salam";

            case "en":
            return "Hello from"+dto.getName();

        default :
            return "Language not found";
    }
    }
}
